package ru.kpfu.itis.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

//
//@Entity
//@Table(name = "employment")
public class Employment {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "id", unique = true, nullable = false)
    private long id;

   // @NotNull
    private String user_id;

    //@NotNull
    private int room_id;

    private Date data_begin;

    private Date data_end;

    public Date getData_end() {
        return data_end;
    }

    public void setData_end(Date data_end) {
        this.data_end = data_end;
    }

    public Date getData_begin() {
        return data_begin;
    }

    public void setData_begin(Date data_begin) {
        this.data_begin = data_begin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }
}
