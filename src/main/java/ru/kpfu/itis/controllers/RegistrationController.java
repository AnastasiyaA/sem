package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.UserRepository;
import ru.kpfu.itis.models.User;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    private UserRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/newUser", method = RequestMethod.GET)
    public ModelAndView addNewUser() {
        ModelAndView modelAndView = new ModelAndView("registryForm");
        User user = new User();
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView addingUser(
            @ModelAttribute("user")
            @Valid User user,
            BindingResult bindingResult) {

        if(bindingResult.hasErrors()){
            return new ModelAndView("registryForm");
        }

        else {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            usersRepository.addUser(user);
            return new ModelAndView("redirect:/");
        }

    }

}
