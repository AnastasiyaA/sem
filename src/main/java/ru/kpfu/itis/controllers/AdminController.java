package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.UserRepository;
import ru.kpfu.itis.models.User;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserRepository usersRepository;

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied(Principal user) {
        ModelAndView model = new ModelAndView();
        if (user != null) {
            model.addObject("msg", "Hi " + user.getName()
                    + ", you do not have permission to access this page!");
        } else {
            model.addObject("msg", "You do not have permission to access this page!");
        }
        model.setViewName("403");
        return model;

    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("mess", "Page is for ADMIN");

        List<User> listUser = usersRepository.list();
        model.addObject("listUser", listUser);

        model.setViewName("admin");
        return model;
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public ModelAndView deleteUser(HttpServletRequest request) {
        String username = request.getParameter("username");
        usersRepository.delete(username);
        return new ModelAndView("redirect:/admin");
    }

    @RequestMapping(value = "/deleteRole", method = RequestMethod.GET)
    public ModelAndView deleteRole(HttpServletRequest request) {
        String authority = request.getParameter("authority");
        usersRepository.deleteRole(authority);
        return new ModelAndView("redirect:/admin");
    }

}
