package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.EmploymentRepository;
import ru.kpfu.itis.dao.RoomRepository;
import ru.kpfu.itis.models.Employment;
import ru.kpfu.itis.models.Room;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class EmploymentController {

    @Autowired
    private EmploymentRepository employmentRepository;
    @Autowired
    private RoomRepository roomRepository;


    @RequestMapping(value = "/rooms/book", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public String showAll(ModelMap map) {
        List<Employment> employments = employmentRepository.list();
        map.put("employments", employments);
        return "rooms/book";
    }

    @RequestMapping(value = "rooms/{id:[1-9]+[0-9]*}/newBook", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_USER')")
    public ModelAndView book(@PathVariable long id) {
        ModelAndView modelAndView1 = new ModelAndView("rooms/newBook");
        Room room = roomRepository.find(id);
        if(room.getStatus().equals("free")){
            modelAndView1.addObject("room", room);
            Employment employment = new Employment();
            modelAndView1.addObject("employment", employment);
            return modelAndView1;
        }else {
            return new ModelAndView("error");
        }
    }

    @RequestMapping(value = "rooms/{id:[1-9]+[0-9]*}/saveBook", method = RequestMethod.POST)
    @PreAuthorize(value="hasRole('ROLE_USER')")
    public ModelAndView bookRoom(@ModelAttribute("employment") Employment employment, @PathVariable long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        employment.setUser_id(username);
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        dateFormat.setLenient(false);
//        String data_begin = request.getParameter("data_begin");
//        String data_end = request.getParameter("data_end");
//        java.util.Date date1 = null;
//        java.sql.Date dateBeg = null;
//        if(data_begin != null && !data_begin.isEmpty()){
//            date1 = dateFormat.parse(data_begin);
//            dateBeg = new java.sql.Date(date1.getTime());
//        }
        Room room = roomRepository.find(id);
        employmentRepository.book(employment,username,room);
//        ,data_begin, data_end
        return new ModelAndView("redirect:/rooms/{id}");
    }

}


