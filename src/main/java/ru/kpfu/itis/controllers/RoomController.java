package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.RoomRepository;
import ru.kpfu.itis.models.Room;

import java.util.List;
import java.util.Random;


@Controller
public class RoomController {

    @Autowired
    private RoomRepository roomRepository;

    @RequestMapping(value = "/rooms", method = RequestMethod.GET)
    public String showAll(ModelMap map) {
        List<Room> rooms = roomRepository.list();
        map.put("rooms", rooms);
        return "rooms/all_rooms";
    }

    @RequestMapping(value = "/newRoom", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ModelAndView addNewRoom() {
        ModelAndView modelAndView = new ModelAndView("rooms/create_room");
        Room room = new Room();
        modelAndView.addObject("room", room);
        return modelAndView;
    }

    @RequestMapping(value = "/saveRoom", method = RequestMethod.POST)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ModelAndView addRoom(@ModelAttribute("room") Room room) {
        Random ran = new Random();
        int image_id = ran.nextInt(15)+1;
        room.setImage_id(image_id);
        roomRepository.addRoom(room);
        return new ModelAndView("redirect:/rooms");
    }

    @RequestMapping(value = "/rooms/{id:[1-9]+[0-9]*}", method = RequestMethod.GET)
    public String show(ModelMap map, @PathVariable long id) {
        Room room = roomRepository.find(id);
        List <Room> rooms = roomRepository.list();
        map.put("room", room);
        map.put("rooms", rooms);
        return "rooms/show";
    }

    @RequestMapping(value = "/rooms/{id:[1-9]+[0-9]*}/delete", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ModelAndView delete(@PathVariable long id){
        roomRepository.deleteRoom(id);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/rooms/{id:[1-9]+[0-9]*}/edit", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ModelAndView edit(@PathVariable long id){
        ModelAndView modelAndView = new ModelAndView("rooms/edit");
        Room room = roomRepository.find(id);
        modelAndView.addObject("room", room);
        return modelAndView;
    }

    @RequestMapping(value = "/rooms/{id:[1-9]+[0-9]*}/edit", method = RequestMethod.POST)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ModelAndView update(@ModelAttribute("room")Room room){
        roomRepository.updateRoom(room);
        return new ModelAndView("redirect:/");
    }
}