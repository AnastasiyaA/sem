package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.itis.dao.UserRepository;
import ru.kpfu.itis.models.User;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserRepository usersRepository;


    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public ModelAndView redirect() {
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public String showAll(ModelMap map) {
        List<User> users = usersRepository.list();
        map.put("users", users);
        return "all_users";
    }

}
