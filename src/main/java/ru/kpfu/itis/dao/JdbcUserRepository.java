package ru.kpfu.itis.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.models.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcUserRepository implements UserRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

//    @Override
//    public User find(int id) {
//        User user = jdbcTemplate.queryForObject(
//                "select * from users where id = ?",
//                new Object[]{id},
//                new RowMapper<User>() {
//                    @Override
//                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
//                        User user = new User();
//                        user.setUsername(rs.getString("username"));
//                        return user;
//                    }
//                }
//        );
//        return user;
//    }

    @Override
    public List<User> list() {
        String sql = "select * from users, authorities where users.username = authorities.username";
        List<User> listUser = jdbcTemplate.query(sql, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setAuthority(rs.getString("authority"));
                return user;
            }
        });
        return listUser;
    }

    @Override
    public void addUser(User user) {
        String sql = "INSERT INTO users (username, password, enabled) VALUES (?, ?, 'TRUE')";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword());
        sql = "INSERT INTO authorities (username, authority) VALUES (?,'ROLE_USER')";
        jdbcTemplate.update(sql, user.getUsername());
    }

    @Override
    public void delete(String username) {
        String sql = "DELETE FROM authorities WHERE username=?";
        jdbcTemplate.update(sql, username);
        sql = "DELETE FROM users WHERE username=?";
        jdbcTemplate.update(sql, username);
    }

    @Override
    public void addRole(User user) {
        String sql = "INSERT INTO authorities (username, authority) VALUES (?,'ROLE_USER')";
        jdbcTemplate.update(sql, user.getUsername());
    }

    @Override
    public void deleteRole(String authority) {
        String sql = "DELETE FROM authorities WHERE authority=?";
        jdbcTemplate.update(sql, authority);
//        String sql2 = "INSERT INTO authorities (username, authority) VALUES (?,'NULL')";
//        jdbcTemplate.update(sql2,authority);

    }

}
