package ru.kpfu.itis.dao;

import ru.kpfu.itis.models.Room;

import java.util.List;

public interface RoomRepository {
    public List<Room> list();
    public void addRoom(Room room);
    public Room find(long id);
    public void deleteRoom(long id);
    public void updateRoom(Room room);
}
