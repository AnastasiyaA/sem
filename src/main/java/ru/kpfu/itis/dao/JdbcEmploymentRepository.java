package ru.kpfu.itis.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.models.Employment;
import ru.kpfu.itis.models.Room;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Repository
public class JdbcEmploymentRepository implements EmploymentRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Employment> list() {
        String sql = "select * from employments";
        List<Employment> employments = jdbcTemplate.query(sql, new RowMapper<Employment>() {

            @Override
            public Employment mapRow(ResultSet rs, int rowNum) throws SQLException {
                Employment employment = new Employment();
                employment.setUser_id(rs.getString("user_id"));
                employment.setRoom_id(rs.getInt("room_id"));
                return employment;
            }
        });
        return employments;
    }

    @Override
    public void book(Employment employment, String username,Room room) {

        String sql = "INSERT INTO employments (id, user_id, room_id) VALUES (default, ?, ?)";
        jdbcTemplate.update(sql, username, room.getId());
        String sql1 = "UPDATE rooms SET status = 'busy' where id = ?";
        jdbcTemplate.update(sql1,room.getId());
    }
}
