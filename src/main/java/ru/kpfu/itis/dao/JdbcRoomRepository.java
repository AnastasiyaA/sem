package ru.kpfu.itis.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.models.Room;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcRoomRepository implements RoomRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Room> list() {
        String sql = "select * from rooms";
        List<Room> roomListList = jdbcTemplate.query(sql, new RowMapper<Room>() {

            @Override
            public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
                Room room = new Room();
                room.setId(rs.getInt("id"));
                room.setCapacity(rs.getInt("capacity"));
                room.setNumber(rs.getInt("number"));
                room.setDescription(rs.getString("description"));
                room.setImage_id(rs.getInt("image_id"));
                room.setStatus(rs.getString("status"));
                return room;
            }
        });
        return roomListList;
    }

    @Override
    public void addRoom(Room room) {
        String sql = "INSERT INTO rooms (id, number, capacity, description, image_id,status) VALUES (default, ?, ?, ?, ?, 'free')";
        jdbcTemplate.update(sql, room.getNumber(), room.getCapacity(), room.getDescription(), room.getImage_id());
    }

    @Override
    public Room find(long id) {
        Room room = jdbcTemplate.queryForObject(
                "select * from rooms where id = ?",
                new Object[]{id},
                new RowMapper<Room>() {
                    @Override
                    public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Room room = new Room();
                        room.setId(rs.getInt("id"));
                        room.setNumber(rs.getInt("number"));
                        room.setDescription(rs.getString("description"));
                        room.setCapacity(rs.getInt("capacity"));
                        room.setImage_id(rs.getInt("image_id"));
                        room.setStatus(rs.getString("status"));
                        return room;
                    }
                });
        return room;
    }

    @Override
    public void deleteRoom(long id) {
        String sql = "DELETE FROM rooms WHERE id = ?";
        jdbcTemplate.update(sql,id);
    }

    @Override
    public void updateRoom(Room room) {
        String sql = "UPDATE rooms SET number = ?, description = ?, capacity = ? WHERE id = ?";
        jdbcTemplate.update(sql,room.getNumber(),room.getDescription(),room.getCapacity(),room.getId());
    }

}
