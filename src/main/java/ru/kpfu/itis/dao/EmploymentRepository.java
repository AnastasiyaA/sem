package ru.kpfu.itis.dao;

import ru.kpfu.itis.models.Employment;
import ru.kpfu.itis.models.Room;

import java.util.Date;
import java.util.List;

public interface EmploymentRepository {
    public List<Employment> list();
    public void book(Employment employment, String username,Room room);

}
