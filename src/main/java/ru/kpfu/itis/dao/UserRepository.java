package ru.kpfu.itis.dao;

import ru.kpfu.itis.models.User;
import java.util.List;

public interface UserRepository{
    public List<User> list();
    public void addUser(User user);
    public void delete(String username);
    public void addRole(User user);
    public void deleteRole(String authority);
}
