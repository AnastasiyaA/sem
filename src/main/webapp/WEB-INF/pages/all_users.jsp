<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="All users">
    <c:forEach items="${users}" var="user">
        <h3>${user.username}</h3>
    </c:forEach>
</t:applicationLayout>