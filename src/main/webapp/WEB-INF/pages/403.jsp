<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Room">
    <img src="/resources/images/errors/error.jpg" alt="403"></p>
    <h2>${msg}</h2>
    <a href="/rooms/${room.id}">Back</a>
</t:applicationLayout>