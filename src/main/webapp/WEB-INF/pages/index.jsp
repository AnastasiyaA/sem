<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Hello">
<h1>${mess}</h1>
<h1>${home}</h1>

<sec:authorize access="isAnonymous()">
    <a href="/newUser">Registration</a>
    <br/>
    <a href="/login">Login</a>
</sec:authorize>

<br/>
<sec:authorize access="isAuthenticated()">
    <li class="mega-menu">
        <a class="sf-with-ul" href="/logout">
            <div>Logout</div>
        </a>
    </li>
    <br/>
    <a href="/users">All users</a>
</sec:authorize>

</t:applicationLayout>