<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Admin">

    <div class="row">
        <%--<div class="col-md-6"><h3>Users</h3></div>--%>
        <br/>
            ${searchUserResult}
        <br/>
        <table class="table table-hover">
            <th><h4>User</h4></th>
            <th><h4>Password</h4></th>
            <th><h4>Role</h4></th>
            <th><h4>Delete</h4></th>
            <c:forEach var="user" items="${listUser}" varStatus="status">
                <tr>
                    <td>${user.username}</td>
                    <td>${user.password}</td>
                    <td>${user.authority}</td>
                    <td>
                        <a href="/deleteUser?username=${user.username}">Delete</a>
                            <%--<a href="/deleteRole?authority=${user.authority}">Delete role</a>--%>
                    </td>
                </tr>
            </c:forEach>
        </table>

    </div>

</t:applicationLayout>