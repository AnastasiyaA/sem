<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:applicationLayout title="All rooms">

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar nobottommargin clearfix">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">

                            <h4>Search</h4>

                            <%--<form:form id="new-room-form" class="nobottommargin" method="POST" commandName="room" action="/saveRoom">--%>
                                <%--<div class="col_full">--%>
                                    <%--<label class="control-label" for="room_capacity">Capacity</label>--%>
                                    <%--<form:select path="capacity" type="text" id="room_capacity" name="room_capacity" value="" class="form-control">--%>
                                        <%--<form:option value="1">1</form:option>--%>
                                        <%--<form:option value="2">2</form:option>--%>
                                        <%--<form:option value="3">3</form:option>--%>
                                        <%--<form:option value="4">4</form:option>--%>
                                        <%--<form:option value="5">5</form:option>--%>
                                    <%--</form:select>--%>
                                <%--</div>--%>

                                <%--<div class="col_full nobottommargin">--%>
                                    <%--<button class="button button-3d nomargin" id="register-form-submit"--%>
                                            <%--name="register-form-submit" value="register">Search--%>
                                    <%--</button>--%>
                                <%--</div>--%>

                                <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
                            <%--</form:form>--%>

                            <div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="613394@N22" data-count="16" data-type="group" data-lightbox="gallery"></div>

                        </div>


                    </div>

                </div><!-- .sidebar end -->

                <!-- Post Content
                ============================================= -->
                <div class="postcontent bothsidebar nobottommargin">

                    <!-- Portfolio Filter
                    ============================================= -->

                    <div class="clear"></div>

                    <!-- Portfolio Items
                    ============================================= -->
                    <div id="portfolio" class="portfolio-1 clearfix">

                        <c:forEach items="${rooms}" var="room">

                            <article class="portfolio-item pf-media pf-icons clearfix">
                                <div class="portfolio-image">
                                    <a href="/rooms/${room.id}">
                                        <img src="/resources/images/${room.image_id}.jpg" alt="Open Imagination">
                                    </a>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="/rooms/${room.id}">${room.number}</a></h3>
                                    <span><a href="#">Количество мест - ${room.capacity}</a></span>
                                    <span><a>Состояние - ${room.status}</a></span>
                                    <p>${room.description}</p>
                                    <ul class="iconlist">
                                        <li><i class="icon-ok"></i> <strong>Created Using:</strong> PHP, HTML5, CSS3</li>
                                        <li><i class="icon-ok"></i> <strong>Completed on:</strong> 12th January, 2014</li>
                                        <li><i class="icon-ok"></i> <strong>By:</strong> John Doe</li>
                                    </ul>
                                </div>
                            </article>

                        </c:forEach>

                    </div><!-- #portfolio end -->

                </div><!-- .postcontent end -->

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar nobottommargin col_last">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">

                            <h4>Instagram Photos</h4>
                            <div id="instagram-widget" class="instagram-photos masonry-thumbs" data-user="envato" data-count="16" data-type="user"></div>

                        </div>

                    </div>
                </div><!-- .sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->
</t:applicationLayout>
