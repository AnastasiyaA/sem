<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:applicationLayout title="Room">
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="postcontent nobottommargin col_last clearfix">

                    <div class="single-post nobottommargin">

                        <div class="entry clearfix">
                            <div class="entry-title">
                                <h2>${room.number} </h2>
                            </div>
                            </br>
                            <div class="entry-image">
                                <img src="/resources/images/${room.image_id}.jpg">
                            </div>
                            <div class="entry-content notopmargin">
                                <p>Вместимость комнаты - ${room.capacity} чел</p>
                                <p>Состояние - ${room.status}</p>
                                <blockquote><p>${room.description}</p></blockquote>
                                <div class="clear"></div>
                            </div>

                            <a href="/rooms/${room.id}/newBook" class="btn btn-info">Book</a>
                            <a href="/rooms/${room.id}/edit" class="btn btn-success">Edit</a>
                            <a href="/rooms/${room.id}/delete" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>

                <div class="sidebar nobottommargin clearfix">

                    <div class="sidebar-widgets-wrap">

                        <div class="widget clearfix">

                            <h4>Other Rooms</h4>

                            <div id="post-list-footer">
                                <c:forEach items="${rooms}" var="r">
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href=${r.id} class="nobg"><img src="/resources/images/${r.image_id}.jpg" alt=""></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="/rooms/${r.id}">${r.number}</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>Вместимость - ${r.capacity}</li>

                                            </ul>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</t:applicationLayout>