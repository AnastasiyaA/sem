<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Employment">
    <table class="table table-hover">
        <th><h4>User</h4></th>
        <th><h4>Room</h4></th>
        <c:forEach items="${employments}" var="employment">
            <tr>
                <td>${employment.user_id}</td>
                <td>${employment.room_id}</td>
            </tr>
        </c:forEach>
    </table>
</t:applicationLayout>