<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:applicationLayout title="Edit room">
  <div class="container clearfix">
    <div class="tabs divcenter nobottommargin clearfix">
      <div class="tab-container">
        <div class="tab-content clearfix" id="tab-register">
          <div class="panel panel-default ">
            <div class="panel-body" style="padding: 40px;">
              <h1>New Room</h1>

              <form:form id="new-room-form" class="nobottommargin" method="POST" commandName="room" action="/rooms/${room.id}/edit">

                <div class="col_full">
                  <label class="control-label" for="room_number">Number</label>
                  <form:input path="number" type="text" id="room_number" name="room_number" value="${room.number}" class="form-control"></form:input>
                </div>

                <div class="col_full">
                  <label class="control-label" for="room_description">Description</label>
                  <form:textarea path="description"  id="room_description" name="room_description" value="${room.description}" class="form-control"></form:textarea>
                </div>

                <div class="col_full">
                  <label class="control-label" for="room_capacity">Capacity</label>
                  <form:select path="capacity" type="text" id="room_capacity" name="room_capacity" value="${room.capacity}" class="form-control">
                    <form:option value="1">1</form:option>
                    <form:option value="2">2</form:option>
                    <form:option value="3">3</form:option>
                    <form:option value="4">4</form:option>
                    <form:option value="5">5</form:option>
                  </form:select>
                </div>

                <div class="col_full nobottommargin">
                  <button class="button button-3d nomargin" id="register-form-submit"
                          name="register-form-submit" value="register">Save
                  </button>
                </div>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
              </form:form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</t:applicationLayout>