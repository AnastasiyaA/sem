<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Book">
    <form:form id="new-room-form" class="nobottommargin" method="POST" commandName="room" action="/rooms/${room.id}/saveBook">

        <p>Are you sure?</p>
        <a href="/rooms/${room.id}" class="btn btn-info">Back</a>

        </br>

        <%--<div class="calendar_">--%>
            <%--<form name="calendar">--%>

                <%--<p>select the start date:--%>
                    <%--<input type="date" name="data_begin"></p>--%>

                <%--<p>select the end date:--%>
                    <%--<input type="date" name="data_end"></p>--%>

                <%--&lt;%&ndash;<input type="submit" value="book">&ndash;%&gt;--%>
            <%--</form>--%>
        <%--</div>--%>

        <div class="col_full nobottommargin">
            <button class="btn btn-success">Book</button>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form:form>
</t:applicationLayout>