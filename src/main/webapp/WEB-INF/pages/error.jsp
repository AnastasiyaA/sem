<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:applicationLayout title="Error">
    <p>Room occupied. But you can choose another room</p>
    <a href="/rooms" class="btn btn-info">All rooms</a>
</t:applicationLayout>
