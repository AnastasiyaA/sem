<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="SemiColonWeb"/>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/bootstrap.css" type="text/css"/>
    <%--<link rel="stylesheet" href="/resources/canvas/style.css" type="text/css" />--%>

    <!-- One Page Module Specific Stylesheet -->
    <link rel="stylesheet" href="/resources/canvas/css/onepage.css" type="text/css"/>
    <!-- / -->

    <link rel="stylesheet" href="/resources/canvas/style.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/et-line.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/magnific-popup.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/fonts1.css" type="text/css"/>
    <link rel="stylesheet" href="/resources/canvas/css/responsive.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript" src="/resources/js/jquery.js"></script>
    <script type="text/javascript" src="/resources/js/plugins.js"></script>
    <title>Hotel :: Hello</title>
</head>

<body class="stretched side-push-panel">

<div class="body-overlay"></div>

<div id="side-panel" class="dark">

    <div id="side-panel-trigger-close"><a href="#"><i class="icon-line-cross"></i></a></div>

    <div class="side-panel-wrap">

        <div class="widget widget_links clearfix">

            <h4>About Me</h4>

            <div style="font-size: 14px; line-height: 1.7;">
                <address style="line-height: 1.7;">
                    KFU, ITIS<br>
                    Alimova Anastasia<br>
                </address>

                <div class="clear topmargin-sm"></div>

                <abbr title="Phone Number">Phone:</abbr> (919) 687 02 69<br>
                <abbr title="Email Address">Email:</abbr> nastya17let@gmail.com
            </div>

        </div>

        <div class="widget quick-contact-widget clearfix">

            <h4>Connect Socially</h4>

            <a href="#" class="social-icon si-small si-colored si-facebook" title="Facebook">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-twitter" title="Twitter">
                <i class="icon-twitter"></i>
                <i class="icon-twitter"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-github" title="Github">
                <i class="icon-github"></i>
                <i class="icon-github"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-pinterest" title="Pinterest">
                <i class="icon-pinterest"></i>
                <i class="icon-pinterest"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-forrst" title="Forrst">
                <i class="icon-forrst"></i>
                <i class="icon-forrst"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-linkedin" title="LinkedIn">
                <i class="icon-linkedin"></i>
                <i class="icon-linkedin"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-gplus" title="Google Plus">
                <i class="icon-gplus"></i>
                <i class="icon-gplus"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-instagram" title="Instagram">
                <i class="icon-instagram"></i>
                <i class="icon-instagram"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-flickr" title="Flickr">
                <i class="icon-flickr"></i>
                <i class="icon-flickr"></i>
            </a>
            <a href="#" class="social-icon si-small si-colored si-skype" title="Skype">
                <i class="icon-skype"></i>
                <i class="icon-skype"></i>
            </a>

        </div>

    </div>

</div>

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="full-header transparent-header dark static-sticky" data-sticky-class="not-dark">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
                        <li class="mega-menu"><a class="sf-with-ul" href="/rooms">
                            <div>All Rooms</div>
                        </a></li>

                        <sec:authorize access="isAnonymous()">

                            <li class="mega-menu"><a class="sf-with-ul" href="/newUser">
                                <div>Sign up</div>
                            </a></li>
                            <li class="mega-menu"><a class="sf-with-ul" href="/login">
                                <div>Sign in</div>
                            </a></li>

                        </sec:authorize>

                        <sec:authorize access="isAuthenticated()">
                            <li class="mega-menu"><a class="sf-with-ul" href="/newRoom">
                                <div>New Room</div>
                            </a></li>
                            <li class="mega-menu"><a class="sf-with-ul" href="/logout">
                                <div>Logout</div>
                            </a></li>
                        </sec:authorize>

                    </ul>

                    <div id="side-panel-trigger"><a href="#"><i class="icon-reorder"></i></a></div>

                </nav>
                <!-- #primary-menu end -->

            </div>

        </div>

    </header>
    <!-- #header end -->

    <section id="slider" class="slider-parallax full-screen force-full-screen">

        <div class="full-screen force-full-screen dark section nopadding nomargin noborder ohidden">

            <div class="container center">
                <div class="vertical-middle ignore-header">
                    <div class="emphasis-title">
                        <h1>
								<span class="text-rotater nocolor" data-separator="|" data-rotate="fadeIn"
                                      data-speed="6000">
									<span class="t-rotate t700 font-body opm-large-word">canvas.|different.|creative.|digital.|vision.</span>
								</span>
                        </h1>
                    </div>
                    <a href="/rooms" class="button button-border button-light button-rounded"
                       data-scrollto="#section-works" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">View
                        our Rooms</a>
                </div>
            </div>

            <div class="video-wrap">
                <video poster="/resources/images/videos/1.jpg" preload="auto" loop autoplay muted>
                    <source src='/resources/images/videos/1.webm' type='video/webm'/>
                    <source src='/resources/images/videos/1.mp4' type='video/mp4'/>
                </video>
                <div class="video-overlay" style="background-color: rgba(0,0,0,0.2);"></div>
            </div>

        </div>

    </section>
    <!-- #slider end -->
    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark noborder">

        <div id="copyrights">
            <div class="container center clearfix">
                Copyright Canvas 2015 | All Rights Reserved
            </div>
        </div>

    </footer>
    <!-- #footer end -->

</div>
<!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/resources/js/functions.js"></script>

</body>

</html>